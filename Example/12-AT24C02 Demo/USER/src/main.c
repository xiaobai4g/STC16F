/*********************************************************************************************************************
 * COPYRIGHT NOTICE
 * Copyright (c) 2020,逐飞科技
 * All rights reserved.
 * 技术讨论QQ群：一群：179029047(已满)  二群：244861897(已满)  三群：824575535
 *
 * 以下所有内容版权均属逐飞科技所有，未经允许不得用于商业用途，
 * 欢迎各位使用并传播本程序，修改内容时必须保留逐飞科技的版权声明。
 *
 * @file       		main
 * @company	   		成都逐飞科技有限公司
 * @author     		逐飞科技(QQ790875685)
 * @version    		查看doc内version文件 版本说明
 * @Software 		MDK FOR C251 V5.60
 * @Target core		STC16F40K128
 * @Taobao   		https://seekfree.taobao.com/
 * @date       		2020-12-18
 ********************************************************************************************************************/

#include "headfile.h"


/*
 *关于内核频率的设定，可以查看board.h文件
 *在board_init中,已经将P54引脚设置为复位
 *如果需要使用P54引脚,可以在board.c文件中的board_init()函数中删除SET_P54_RESRT即可
 */


//AT24C02是一个外置EEPROM模块，他支持IIC总线操作。


uint8 write_dat[10];
uint8 read_dat[10];
uint8 i;
void main()
{
	DisableGlobalIRQ();		//关闭总中断

	board_init();			//初始化寄存器

	for(i=0; i<10; i++)
	{
		write_dat[i] = 40+i;	//赋值
	}
	
	for(i=0; i<10; i++)
	{
		//从地址i开始，写入i，单字节写入10个字节
		at24c02_write_byte(i, i+20);	
	}
	
	
	//从地址100开始多字节写入10个字节
	at24c02_write_bytes(100, write_dat, 10);


	//总中断最后开启
	EnableGlobalIRQ();		//开启总中断
    while(1)
	{
		//读取10个字节，一次读取一个。一共读取10次。
		for(i=0; i<10; i++)
		{
			printf("byte = %d\r\n", at24c02_read_byte(i));
		}
		delay_ms(500);
		
		//从地址100开始多字节读取10个字节
		at24c02_read_bytes(100, read_dat, 10);
		for(i=0; i<10; i++)
		{
			//打印输出当前读取到的数据
			printf("read_dat = %d\r\n", read_dat[i]);
		}
		delay_ms(500);
    }
}

